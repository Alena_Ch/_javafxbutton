package by.stormnet.figuresfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FiguresApp extends Application{
    private boolean on;

    public static void main(String[] args) {
        launch();

    }

    @Override
    public void start(Stage window) throws Exception {
        window.setScene(new Scene(
                FXMLLoader.load(getClass().getResource("/views/MainScreenView.fxml")),
                1024.0,
                600.0
                )
        );
        window.setTitle("First FX Program");
        window.show();



//        AnchorPane root = new AnchorPane();
//        Button btn = new Button("Push me");
//        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                System.out.println("button clicked");
//                if (!on) {
//                    root.setStyle("-fx-background-color: green");
//                } else {
//                    root.setStyle("-fx-background-color: white");
//            }
//                on = !on;
//            }
//        });
//        AnchorPane.setBottomAnchor(btn,10.0);
//        AnchorPane.setRightAnchor(btn,10.0);
//        root.getChildren().add(btn);
//
//        window.setScene(new Scene(root,800,600));
//        window.setTitle("My First FX App");
//
//        window.show();
    }
}
